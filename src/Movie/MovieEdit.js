import React, {useEffect, useState} from "react";
import axios from "axios";
import './MovieCreate.css'
import {useHistory, useParams} from "react-router-dom";

const MovieEdit = () => {
    const [daftarMovie, ] = useState(null)
    const [inputTitle, setInputTitle] = useState("")
    const [inputDescription, setInputDescription] = useState("")
    const [inputDuration, setInputDuration] = useState("")
    const [inputGenre, setInputGenre] = useState("")
    const [inputRating, setInputRating] = useState("")
    const [inputReview, setInputReview] = useState("")
    const [inputUrl, setInputUrl] = useState("")

    // const [selectedId, setSelectedId] = useState(0)
    const [statusForm,] = useState("edit")


    const handleChange = (event) => {
        const {name, value} = event.target

        switch (name) {
            case "inputTitle":
                setInputTitle(value);
                console.log(value)
                break;
            case "inputDescription":
                setInputDescription(value);
                console.log(value)
                break;
            case "inputDuration":
                setInputDuration(value);
                console.log(value)
                break;
            case "inputGenre":
                setInputGenre(value);
                console.log(value)
                break;
            case "inputRating":
                if (value.length <= 2) {
                    setInputRating(value);
                    console.log(value)
                }
                break;
            case "inputReview":
                setInputReview(value);
                console.log(value)
                break;
            case "inputUrl":
                setInputUrl(value);
                console.log(value)
                break;
            default:
                break;
        }
    }

    let history = useHistory();
    let {id} = useParams();
    // console.log(id)

    function handleClick() {
        history.push("/movie-table");
    }

    useEffect(() => {
        if (daftarMovie === null) {
            axios.get(`https://backendexample.sanbersy.com/api/movies/${id}`)
                .then(res => {
                    setInputTitle(res.data.title)
                    setInputDescription(res.data.description)
                    setInputDuration(res.data.duration)
                    setInputGenre(res.data.genre)
                    setInputRating(res.data.rating)
                    setInputReview(res.data.review)
                    setInputUrl(res.data.image_url)
                })
        }
    })

    const handleSubmit = (event) => {
        // menahan submit
        event.preventDefault()

        let title = inputTitle
        let description = inputDescription
        let duration = inputDuration
        let genre = inputGenre
        let rating = inputRating
        let review = inputReview
        let image_url = inputUrl

        if (statusForm === "edit") {
            axios.put(` https://backendexample.sanbersy.com/api/movies/${id}`, {
                title,
                description,
                duration,
                genre,
                rating,
                review,
                image_url
            })
            .then(res => {
                handleClick()
                console.log(res)
            })
        }
    }

    return (
        <form style={{
            marginTop: "1rem", padding: "2rem",
            display: "flex",
            justifyContent: "center",
            flexFlow: "column", alignItems: "center"
        }} onSubmit={handleSubmit}>
            <h1>Form Daftar Film</h1>
            <div className={"wrapper-input"}>
                <label>Title :</label>
                <input type={"text"} onChange={handleChange} name="inputTitle" value={inputTitle}/>
            </div>
            <div className={"wrapper-input"}>
                <label>Deskripsi :</label>
                <input type={"text"} onChange={handleChange} name="inputDescription"
                       value={inputDescription}/>
            </div>
            <div className={"wrapper-input"}>
                <label>Durasi :</label>
                <input type={"number"} onChange={handleChange} name="inputDuration" value={inputDuration}/>
            </div>
            <div className={"wrapper-input"}>
                <label>Genre :</label>
                <input type={"text"} onChange={handleChange} name="inputGenre" value={inputGenre}/>
            </div>
            <div className={"wrapper-input"}>
                <label>Rating :</label>
                <input type={"number"} min="1" max="10" onChange={handleChange} name="inputRating"
                       value={inputRating}/>
            </div>
            <div className={"wrapper-input"}>
                <label>Review :</label>
                <input type={"text"} onChange={handleChange} name="inputReview" value={inputReview}/>
            </div>
            <div className={"wrapper-input"}>
                <label>Url :</label>
                <input type={"text"} onChange={handleChange} name="inputUrl" value={inputUrl}/>
            </div>
            <button style={{width: "10rem", marginTop: "1rem"}} type="submit" value="submit">Submit</button>
        </form>
    )
}
export default MovieEdit