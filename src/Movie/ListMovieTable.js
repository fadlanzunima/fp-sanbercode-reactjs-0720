import React, {Component} from "react";
import axios from "axios";
import './ListMovie.css';

import 'antd/dist/antd.css';
import {Modal, Table, Input, Button, Space} from 'antd';
import Highlighter from 'react-highlight-words';
import {SearchOutlined} from '@ant-design/icons';
import {Link} from "react-router-dom";


class ListMovieTable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            movies: [],
            detailMovie: [],
            visible: false,
            searchText: '',
            searchedColumn: ''
        }
    }

    handleOk = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    getColumnSearchProps = dataIndex => ({
        filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
            <div style={{padding: 8}}>
                <Input
                    ref={node => {
                        this.searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{width: 188, marginBottom: 8, display: 'block'}}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined/>}
                        size="small"
                        style={{width: 90}}
                    >
                        Search
                    </Button>
                    <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{width: 90}}>
                        Reset
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{color: filtered ? '#1890ff' : undefined}}/>,
        onFilter: (value, record) =>
            record[dataIndex] ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()) : '',
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => this.searchInput.select(), 100);
            }
        },
        render: text =>
            this.state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{backgroundColor: '#ffc069', padding: 0}}
                    searchWords={[this.state.searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ''}
                />
            ) : (
                text
            ),
    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    handleReset = clearFilters => {
        clearFilters();
        this.setState({searchText: ''});
    };

    columns = [
        {
            title: 'No',
            dataIndex: 'key',
            sorter: (a, b) => a.key - b.key,
            sortDirections: ['descend'],
        },
        {
            title: 'Judul',
            dataIndex: 'title',
            sorter: (a, b) => a.title.length - b.title.length,
            sortDirections: ['descend'],
            ...this.getColumnSearchProps('title'),
        },
        {
            title: 'Rating',
            dataIndex: 'rating',
            defaultSortOrder: 'descend',
            sorter: (a, b) => a.rating.length - b.rating.length,
            ...this.getColumnSearchProps('rating'),
        },
        {
            title: 'Duration',
            dataIndex: 'duration',
            filterMultiple: false,
            sorter: (a, b) => a.duration - b.duration,
            sortDirections: ['descend', 'ascend'],
        },
        {
            title: 'Genre',
            dataIndex: 'genre',
            sorter: (a, b) => a.genre.length - b.genre.length,
            sortDirections: ['descend', 'ascend'],
        },
        {
            title: 'Description',
            dataIndex: 'description',
            sorter: (a, b) => a.description.length - b.description.length,
            sortDirections: ['descend', 'ascend'],
        },
        {
            title: 'Aksi',
            dataIndex: 'detailButton'
        },
        {
            dataIndex: 'editButton'
        },
        {
            dataIndex: 'deleteButton'
        }
    ];


    onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
    }

    handleDetail = (event) => {
        this.setState({
            visible: true,
        });

        let idMovie = parseInt(event.target.value)
        axios.get(`https://www.backendexample.sanbersy.com/api/movies/${idMovie}`)
            .then(response => {
                console.log(response.data)
                let detailMovie =
                    {
                        title: response.data.title,
                        description: response.data.description,
                        duration: response.data.duration,
                        genre: response.data.genre,
                        image_url: response.data.image_url,
                        rating: response.data.rating,
                        review: response.data.review,
                    }
                this.setState({detailMovie})
            })
    }

    handleDelete = (event) => {
        let idMovie = parseInt(event.target.value)

        let newDaftarMovie = this.state.movies.filter(el => el.id !== idMovie)

        axios.delete(` https://backendexample.sanbersy.com/api/movies/${idMovie}`)
            .then(res => {
                console.log(res)
            })

        this.setState({movies: newDaftarMovie})

    }

    componentDidMount() {
        axios.get(`https://www.backendexample.sanbersy.com/api/movies`)
            .then(res => {
                let movies = res.data.map((el, index) => {
                    return {
                        key: index + 1,
                        id: el.id,
                        title: el.title,
                        rating: el.rating,
                        duration: (el.duration / 60) + ' jam',
                        genre: el.genre,
                        description: el.description,
                        detailButton: <button onClick={this.handleDetail} value={el.id}>Detail</button>,
                        deleteButton: <button onClick={this.handleDelete} value={el.id}
                                              className={"btn-delete"}>Delete</button>,
                        editButton: <button value={el.id}><Link to={"/movie/edit/" + el.id}>Edit</Link></button>,
                    }
                })
                this.setState({movies})
            })
    }

    render() {
        return (
            <section className={"wrapper-list-movie"}>
                <h1>Daftar Film Terbaik</h1>
                <div style={{
                    display: "flex",
                    width: "100%",
                    justifyContent: "flex-end"
                }}>
                    <button><Link to="/movie/create">Tambah Movie</Link></button>
                </div>
                <Table columns={this.columns} dataSource={this.state.movies} onChange={this.onChange}/>
                <Modal
                    title="Detail Movie"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <div>
                        <p>Title : {this.state.detailMovie.title}</p>
                        <p>Rating : {this.state.detailMovie.rating}</p>
                        <p>Durasi : {this.state.detailMovie.duration}</p>
                        <p>Genre : {this.state.detailMovie.genre}</p>
                        <p>Deskripsi : {this.state.detailMovie.description}</p>
                        {this.state.detailMovie.image_url == null ? <p>Tidak ada Gambar</p> :
                            <div className={"wrapper-image"}>
                                <img src={this.state.detailMovie.image_url} alt={"img"}/>
                            </div>
                        }
                    </div>
                </Modal>
            </section>
        )
    }
}

export default ListMovieTable