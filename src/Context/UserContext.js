import React, { useState, createContext } from "react";

export const UserContext = createContext();

export const UserProvider = props => {
  const currentUser = JSON.parse(localStorage.getItem("user"))
  const currentStatus = JSON.parse(localStorage.getItem("regis"))
  const iniateUser = currentUser ? currentUser : null
  const iniateStatus = currentStatus ? currentStatus : null
  const [user, setUser] = useState(iniateUser);
  const [userStatus, setUserStatus] = useState(iniateStatus);

  return (
    <UserContext.Provider value={[user, setUser, userStatus, setUserStatus]}>
      {props.children}
    </UserContext.Provider>
  );
};