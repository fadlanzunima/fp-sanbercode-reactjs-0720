import React from "react";
import MainContent from "./Content";
import {UserProvider} from "../Context/UserContext";


const Section = () => {

    return (
        <UserProvider>
            <MainContent/>
        </UserProvider>
    )
}

export default Section