import React, {useState} from "react";
import axios from "axios";
import '../Movie/MovieCreate.css'
import {useHistory} from "react-router-dom";

const GameCreate = () => {
    // const [daftarMovie, setdaftarMovie] = useState(null)
    const [inputName, setInputName] = useState("")
    const [inputGenre, setInputGenre] = useState("")
    const [inputSinglePlayer, setInputSinglePlayer] = useState("")
    const [inputMultiPlayer, setInputMultiPlayer] = useState("")
    const [inputPlatform, setInputPlatform] = useState("")
    const [inputUrl, setInputUrl] = useState("")

    // const [selectedId, setSelectedId] = useState(0)
    const [statusForm,] = useState("create")


    const handleChange = (event) => {
        const {name, value} = event.target

        switch (name) {
            case "inputName":
                setInputName(value);
                console.log(value)
                break;
            case "inputGenre":
                setInputGenre(value);
                console.log(value)
                break;
            case "inputSinglePlayer":
                if (value <= 1) {
                    setInputSinglePlayer(value);
                }
                console.log(value)
                break;
            case "inputMultiPlayer":
                if (value <= 1){
                setInputMultiPlayer(value);
                }
                console.log(value)
                break;
            case "inputPlatform":
                setInputPlatform(value);
                console.log(value)
                break;
            case "inputUrl":
                setInputUrl(value);
                console.log(value)
                break;
            default:
                break;
        }
    }

    let history = useHistory();

    function handleClick() {
        history.push("/game-table");
    }

    const handleSubmit = (event) => {
        // menahan submit
        event.preventDefault()

        let name = inputName
        let genre = inputGenre
        let singlePlayer = inputSinglePlayer
        let multiplayer = inputMultiPlayer
        let platform = inputPlatform
        let image_url = inputUrl

        if (name.replace(/\s/g, '') !== "" && genre !== "" && singlePlayer !== "" && multiplayer !== ""  && platform !== "" && image_url !== "") {
            if (statusForm === "create") {
                axios.post(`https://backendexample.sanbersy.com/api/games`, {
                    name,
                    genre,
                    singlePlayer,
                    multiplayer,
                    platform,
                    image_url
                })
                    .then(response => {
                        handleClick()
                        console.log(response)
                    })
            }
        }
    }

    return (
        <form style={{
            marginTop: "1rem", padding: "2rem",
            display: "flex",
            justifyContent: "center",
            flexFlow: "column", alignItems: "center"
        }} onSubmit={handleSubmit}>
            <h1>Form Daftar Game</h1>
            <div className={"wrapper-input"}>
                <label>Nama :</label>
                <input type={"text"} onChange={handleChange} name="inputName" value={inputName}/>
            </div>
            <div className={"wrapper-input"}>
                <label>Genre :</label>
                <input type={"text"} onChange={handleChange} name="inputGenre" value={inputGenre}/>
            </div>
            <div className={"wrapper-input"}>
                <label>Single Player:</label>
                <input type={"number"} min={"0"} max={"1"} onChange={handleChange} name="inputSinglePlayer"
                       value={inputSinglePlayer}/>
            </div>
            <div className={"wrapper-input"}>
                <label>Multi Player :</label>
                <input type={"number"} min={"0"} max={"1"} onChange={handleChange} name="inputMultiPlayer" value={inputMultiPlayer}/>
            </div>
            <div className={"wrapper-input"}>
                <label>Platform :</label>
                <input type={"text"} onChange={handleChange} name="inputPlatform"
                       value={inputPlatform}/>
            </div>
            <div className={"wrapper-input"}>
                <label>Url :</label>
                <input type={"text"} onChange={handleChange} name="inputUrl" value={inputUrl}/>
            </div>
            <button style={{width: "10rem", marginTop: "1rem"}} type="submit" value="submit">Submit</button>
        </form>
    )
}
export default GameCreate