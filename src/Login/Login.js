import React, {useContext, useState} from "react";
import './Login.css';
import axios from "axios";
import {UserContext} from "../Context/UserContext";
import {Link} from "react-router-dom";

function Login() {
    const [inputUsername, setInputUsername] = useState("")
    const [inputPassword, setInputPassword] = useState("")

    const [, setUser] = useContext(UserContext);

    const [statusForm, ] = useState("create")

    const handleChange = (event) => {
        const {name, value} = event.target

        switch (name) {
            case "inputUsername":
                setInputUsername(value);
                break;
            case "inputPassword":
                setInputPassword(value);
                break;
            default:
                break;
        }
    }

    const handleSubmit = (event) => {
        // menahan submit
        event.preventDefault()

        let username = inputUsername
        let password = inputPassword

        if (username !== "" && password !== "") {
            if (statusForm === "create") {
                axios.post("https://backendexample.sanbersy.com/api/login", {
                    username,
                    password
                })
                    .then(response => {
                        console.log(response)
                        if(response.status === 200){
                            if(response.data === "invalid username or password"){
                                alert(response.data)
                            }
                            else
                            {
                                setUser("user")
                                localStorage.setItem("user", JSON.stringify({username, password}))
                                console.log("pindah ke menu")
                            }
                        }
                    })
            }
        }
    }

    return (
        <div className={"wrapper-login"}>
            <h1>Welcome</h1>
            <form onSubmit={handleSubmit}>
                <div className={"wrapper-input"}>
                    <label>Username</label>
                    <input type={"text"} onChange={handleChange} name="inputUsername" value={inputUsername}/>
                </div>
                <div className={"wrapper-input"}>
                    <label>Password</label>
                    <input type={"password"} onChange={handleChange} name="inputPassword" value={inputPassword}/>
                </div>
                <div className={"wrapper-button"}>
                    <Link style={{textAlign: "center", marginBottom: "1rem"}} to="/register">Belum Punya akun?</Link>
                    <button className={"btn-login"} type="submit" value="submit">Login</button>
                </div>
            </form>
        </div>
    )
}

export default Login